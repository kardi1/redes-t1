#include "utils.h"

void leComando (int soquete, unsigned char *seq) {
  string comando;
  getline(cin,comando);
  if(comando == "exit") {
    exit(0);
  }else if(comando.front() == 'l') {
    comando.erase(comando.begin());
    if(comando.substr(0,2) == "ls") {
      lsLocal(comando);
    } else if(comando.substr(0,2) == "cd") {
      comando.erase(comando.begin(),comando.begin()+3);
      cdLocal(comando);
    } else {
      cout<<"Comando invalido"<<endl;
      menu();
    }
  }else if(comando.front() == 'r'){
    comando.erase(comando.begin());
    if(comando.substr(0,2) == "ls") {
      comando.erase(comando.begin(), comando.begin()+2);
      lsRemoto(comando,soquete, seq);
    }else if(comando.substr(0,2) == "cd") {
      comando.erase(comando.begin(), comando.begin()+2);
      cdRemoto(comando,soquete, seq);
    } else {
      cout << "Comando Invalido" << endl;
    }
  }else if(comando.substr(0,3) == "put") {
    comando.erase(comando.begin(), comando.begin()+3);
    while(comando.front() == ' ') comando.erase(comando.begin());
    putPacote(comando, soquete, seq);
  }else if(comando.substr(0,3) == "get") {
    comando.erase(comando.begin(), comando.begin()+3);
    while(comando.front() == ' ') comando.erase(comando.begin());
    getPacote(comando, soquete, seq);
  }else {
    cout<<"Comando invalido"<<endl;
  }
}

void lsLocal (string argumento) {
  system(argumento.c_str());
}

void cdLocal (string argumento) {
  chdir(argumento.c_str());
}

void lsRemoto (string argumento, int soquete, unsigned char *seq) {
  enviaPac(soquete, argumento,4,seq);
}

void cdRemoto (string argumento, int soquete, unsigned char *seq) {
  enviaPac(soquete, argumento,3,seq);
}

void enviaArquivo(int soquete, ifstream *file, unsigned char *seq, unsigned long long int fileTam){
  char cFileContent[64];
  unsigned long long int pacotes;
  int resto;
  Pacote tmp;

  memset(cFileContent, 0, sizeof(cFileContent));

  pacotes = fileTam / 63;
  resto = fileTam % 63;

  tmp.setSeq(*seq);
  tmp.setType(13);
  tmp.setData("");
  int first, i, fim, repeat, last =0;
  Pacote Q[64];
  //fazer roles de entrar no loop soh com a quantidade certa de pacotes pah
  first = -1;
  while(pacotes > 2){
    repeat =0;
    last = file->tellg();
repeatLabel:
    for(int j=0; j < 3; j++){
      if(repeat){
        cout<<"Alterando apontador do arquivo"<<endl;
        cerr<<"Alterando apontador do arquivo"<<endl;
        file->seekg(last, ios::beg);
        repeat =0;
      }
      tmp.setType(13);
      if (first != -2){
        file->read(cFileContent, sizeof(char)*63);
      }
      if (first == -1 || first == -2) first = *seq;
      tmp.setSeq(*seq);
      (*seq)++;
      tmp.setData(cFileContent,63);
      Q[tmp.getSeq()] = tmp;
      tmp.setCalcCrc();
      tmp.sendPac(soquete);
    }
    checa:
    tmp.getPac(soquete, true);
    if(tmp.getType() == 0 && tmp.getData()[1] == 97){
      cerr<< "Reduzindo o seq de: "<< (int)((*seq)&0x3f);
      (*seq)-= 3;
      (*seq) &= 0x3f;
      cerr<< " para: " << (int)((*seq)&0x3f) << endl;
      repeat = 1;

      goto repeatLabel;
    }
    (*seq)++;
    if(tmp.getType() == 0){
      i = first;
      if(tmp.getData()[0] == i){
      //deu ruim no primeiro
        Q[i].sendPac(soquete);
        goto checa;
      }
      i++;
      i &= 0x3f;
      if(tmp.getData()[0] == i){
      //deu boa no 1
        pacotes--;
      //deu ruim no segundo
      //faz shift da janela
        first = i;
      //reenvia o segundo
        Q[i].sendPac(soquete);
      //envia o quarto, goto
        file->read(cFileContent, sizeof(char)*63);
        tmp.setSeq(*seq);
        (*seq)++;
        tmp.setType(13);
        tmp.setData(cFileContent,63);
        Q[tmp.getSeq()] = tmp;
        tmp.sendPac(soquete);
        goto checa; 
      }
      i++;
      i &= 0x3f;
      if(tmp.getData()[0] == i){
      //deu boa no 1 e no 2
        pacotes -= 2;
      //deu ruim no terceiro
      //seta para first -2 e continua o loop
        first = -2;
      }
    }else{
    //deu boa em todos;
      pacotes -= 3;
    //algo pode ter dado boa
    //assumindo que deu boa mesmo, continua o loop
    //seta first -1
      first = -1;
    }
  }
  fim=0;
  for(unsigned int j=0; j < pacotes; j++){
    tmp.setType(13);
    if (first != -2){
      file->read(cFileContent, sizeof(char)*63);
    }
    if (first == -1 || first == -2) first = *seq;
    tmp.setSeq(*seq);
    (*seq)++;
    tmp.setData(cFileContent,63);
    Q[tmp.getSeq()] = tmp;
    tmp.sendPac(soquete);
    fim++;
  }
  //fazer checagem do maybe
  if ( resto > 0){
    memset(cFileContent, 0, sizeof(cFileContent));
    file->read(cFileContent, sizeof(char)*resto);
    tmp.setSeq(*seq);
    (*seq)++;
    tmp.setData(cFileContent, resto);
    tmp.setCalcCrc();
    Q[tmp.getSeq()] = tmp;
    tmp.sendPac(soquete);
    fim++;
  }

  checa2:
  tmp.getPac(soquete);
  (*seq)++;
  if(tmp.getType() == 0){
    Q[tmp.getData()[0]].sendPac(soquete);
    goto checa2;
  }//else{
  //assumindo boa pra todo mundo xablau
  //ver se é ack do ultimo pacote(first+fim)&0x3f
  //se sim xablau
  //se nao enviar o ack++ e talvez ack++++;
  //}
}

void putPacote (string argumento, int soquete, unsigned char *seq){
  struct stat statusFile;
  ifstream file;
  unsigned long long int fileTam;
  char cFileTam[64];


  if(stat(argumento.c_str(), &statusFile) == -1) {
    cout << "Erro no put, arquivo nao existente ou sem permissoes" << endl;
    return;
  }

  fileTam = statusFile.st_size;
  sprintf(cFileTam,"%lld",fileTam);

  cout << "O arquivo: " << argumento << " tem tamanho: " << cFileTam << endl;

  if (!enviaPac2(soquete,argumento,5,seq)){
    cout << "Erro fatal"<< endl;
    return;
  }
  if (!enviaPac2(soquete,cFileTam,9,seq)){
    cout << "Erro fatal"<< endl;
    return;
  }
  //comeca a transferir os roles;
  file.open(argumento.c_str(), ios::binary);
  enviaArquivo(soquete, &file,seq, fileTam);
  file.close();
  enviaPac(soquete, "",15,seq);
}

void getPacote (string argumento, int soquete, unsigned char *seq){

  ofstream wri;
  wri.open(argumento, ios::binary);

  Pacote ok, rec, error, tmp, nack,ack;

  ack.setType(1);
  ack.setData("");

  ok.setType(8);
  ok.setData("");



  if(wri){
    tmp.setType(6);
    tmp.setSeq(*seq);
    (*seq)++;
    tmp.setData(argumento.c_str());
    tmp.sendPac(soquete);

    rec.getPac(soquete,true);
    (*seq)++;
    while (rec.getType() == 0){
      tmp.setSeq(*seq);
      (*seq)++;
      tmp.setCalcCrc();
      tmp.sendPac(soquete);
      rec.getPac(soquete,true);
      (*seq)++;
    }
    if (rec.getType() != 9){
      cout << "Erro fatal" << endl;
      return;
    }
    cout << "O arquivo: "<< argumento <<" tem tamanho: "<< rec.getData() << endl;
    //extrair tamanho
    ok.setSeq(*seq);
    (*seq)++;
    ok.setCalcCrc();
    ok.sendPac(soquete);

    RecebeArquivo(soquete, &wri,rec.getData(),seq);

    tmp.getPac(soquete,true);
    while (tmp.getType() == 0){
      ack.setSeq(*seq);
      ack.setCalcCrc();
      ack.sendPac(soquete);
      tmp.getPac(soquete,true);
    }
    (*seq)++;

    if(tmp.getType() != 15){
      cout << "Erro fatal" << endl;
    }
    ack.setSeq(*seq);
    (*seq)++;
    ack.setCalcCrc();
    ack.sendPac(soquete);

    wri.close();
  }
}

void menu () {
  cout<<"\t Menu"<<endl;
  cout<<"Comando \t Equivalente"<<endl;
  cout<<"lls \t ls local"<<endl;
  cout<<"lcd \t cd local"<<endl;
  cout<<"rls \t ls remoto"<<endl;
  cout<<"rcd \t cd remoto"<<endl;
  cout<<"put \t\t put"<<endl;
  cout<<"get \t\t get"<<endl;
  cout<<"exit \t\t sair"<<endl;
}
