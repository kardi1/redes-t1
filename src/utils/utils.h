#ifndef UTILS_H
#define UTILS_H
//defaults
#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <iostream>
#include <unistd.h>

#include <sys/stat.h>
#include <fstream>


#include <string>

#include "../network/network.h"

using namespace std;

void menu ();
void lsLocal (string argumento);
void cdLocal (string argumento);
void lsRemoto (string argumento, int soquete, unsigned char *seq);
void cdRemoto (string argumento, int soquete, unsigned char *seq);
void putPacote (string argumento, int soquete, unsigned char *seq);
void getPacote (string argumento, int soquete, unsigned char *seq);
void enviaArquivo(int soquete, ifstream *file, unsigned char *seq, unsigned long long int fileTam);
void leComando (int soquete, unsigned char *seq);
#endif
