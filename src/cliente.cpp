#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/ethernet.h>
#include <linux/if_packet.h>
#include <linux/if.h>

//defaults
#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <iostream>


//error
#include <errno.h>

//sleep
#include <unistd.h>

#include "pacote/pacote.h" 
#include "utils/utils.h"

using namespace std;

int ConexaoRawSocket(char *device)
{
  int soquete;
  struct ifreq ir;
  struct sockaddr_ll endereco;
  struct packet_mreq mr;

  soquete = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));      /*cria socket*/
  if (soquete == -1) {
    printf("Erro no Socket\n");
    exit(-1);
  }

  memset(&ir, 0, sizeof(struct ifreq));     /*dispositivo eth0*/
  memcpy(ir.ifr_name, device, strlen(device));
  if (ioctl(soquete, SIOCGIFINDEX, &ir) == -1) {
    printf("Erro no ioctl\n");
    exit(-1);
  }
    

  memset(&endereco, 0, sizeof(endereco));   /*IP do dispositivo*/
  endereco.sll_family = AF_PACKET;
  endereco.sll_protocol = htons(ETH_P_ALL);
  endereco.sll_ifindex = ir.ifr_ifindex;
  if (bind(soquete, (struct sockaddr *)&endereco, sizeof(endereco)) == -1) {
    printf("Erro no bind\n");
    exit(-1);
  }


  memset(&mr, 0, sizeof(mr));          /*Modo Promiscuo*/
  mr.mr_ifindex = ir.ifr_ifindex;
  mr.mr_type = PACKET_MR_PROMISC;
  if (setsockopt(soquete, SOL_PACKET, PACKET_ADD_MEMBERSHIP, &mr, sizeof(mr)) == -1)    {
    printf("Erro ao fazer setsockopt\n");
    exit(-1);
  }

  return soquete;
}

int main(){
  printf("Começo do main\n");
  //unsigned char seg[10],seq=0,seg1[10];
#ifdef LO_DEF
  char interface[] = "lo";
#else
  char interface[] = "eth0";
  printf("ETH0\n");
#endif
  int soquete = ConexaoRawSocket(interface);
  unsigned char seq;
  seq = 0;
  printf("soquete %d\n", soquete);
  /*
  while(1){
    if ((send (soquete, &buf, sizeof(buf), 0)) == -1) {
      printf("Erro send %s\n",strerror(errno));
      sleep (1);
    }
  }*/

  menu();
  //cout<<"Sequencia::" << (int)seq << endl;

  while(1){
    leComando(soquete,&seq);
    //cout<<"Sequencia::" << (int)seq << endl;
  }

  printf("Fim do main\n");
  return 0;
}
