#ifndef PACOTE_H
#define PACOTE_H

//socket
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/ethernet.h>
#include <linux/if_packet.h>
#include <linux/if.h>
#include <arpa/inet.h>

//defaults
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <string>
#include <cstdio>

//error
#include <errno.h>

//sleep
#include <unistd.h>

#include <poll.h>

using namespace std;

class Pacote {
  private:
    /*struct pack_t {
      unsigned char head[3];
      char *data;
      unsigned char crc;
    };
    struct pack_t pack;*/
    unsigned char packdata[67];
    unsigned char packcrc;
    int packtam;
  
  public:
    Pacote();

    void setTam(int);
    int getTam();

    void setSeq(int);
    int getSeq();
    
    void setType(int);
    int getType();

    void setData(const char *);
    void setData(const char *data,int tam);
    unsigned char* getData();

    void sendPac(int);
    void getPac(int soquete);
    void getPac(int soquete, bool timeout);

    bool isValid();

    int calcCrc();
    void setCalcCrc();

};

#endif
