#include "pacote.h"

Pacote::Pacote() {
    //packdata = (unsigned char*) malloc (sizeof(unsigned char) * 67);
    memset(packdata,0,67*sizeof(unsigned char));
    packdata[0] = 0x7e;
    packcrc = 0;
}

void Pacote::setTam(int tam){
  unsigned char temp;
  //resets tam to 0
  temp = 3;
  packdata[1] = packdata[1] & temp;
  //set tam to tam
  temp = (unsigned char)(tam & 0x3f);
  temp = temp << 2;
  packdata[1] = packdata[1] | temp;
}

int Pacote::getTam(){
  return (packdata[1] & (0x3f << 2))>>2;
}

void Pacote::setSeq(int seq){
  unsigned char temp;
  //reset seq to 0
  temp = 0xFC;
  packdata[1] = packdata[1] & temp;
  temp = 0x0F;
  packdata[2] = packdata[2] & temp;

  //sets seq to seq
  temp = (unsigned char) seq;
  temp = temp >> 4;
  temp = temp & 0x03;
  packdata[1] = packdata[1] | temp;

  temp = (unsigned char) seq;
  temp = temp & 0x0F;
  temp = temp << 4;
  packdata[2] = packdata[2] | temp;
}

int Pacote::getSeq(){
  unsigned char temp;
  temp = 0;
  temp = (packdata[1] & 0x03) << 4;
  temp |= (packdata[2] & 0xF0) >> 4;
  return (int) temp;
}

void Pacote::setType(int type){
  unsigned char temp;
  temp = 0x0F & type;
  packdata[2] = packdata[2] & 0xF0;
  packdata[2] = packdata[2] | temp;
}

int Pacote::getType(){
  unsigned char temp;
  temp = packdata[2] & 0x0F;
  return (int) temp;
}

void Pacote::setData(const char *data){
  //memcpy(pack.data, data, strlen(data));
  //if (pack.data != NULL) free(pack.data);
  //pack.data = (char*) malloc (sizeof(char)*strlen(data));
//  packdata = (unsigned char*) realloc (packdata, sizeof(unsigned char)*(3+strlen(data)));
  packtam = 3+strlen(data)+1;
  this->setTam(packtam-4);
  packcrc = 0;
  packcrc = packdata[1] xor packcrc;
  packcrc = packdata[2] xor packcrc;
  for (unsigned int i = 0; i < strlen(data); i++){
      packdata[i+3] = data[i];
      packcrc = data[i] xor packcrc;
  }
  packdata[packtam-1] = packcrc;
  if (packtam <67){
    for (int i=packtam; i<=67;i++){
      packdata[i]=0x00;
    }
    packtam=67;
  }
  //strcpy(pack.data,data);
}

void Pacote::setData(const char *data,int tam){
  //memcpy(pack.data, data, strlen(data));
  //if (pack.data != NULL) free(pack.data);
  //pack.data = (char*) malloc (sizeof(char)*strlen(data));
//  packdata = (unsigned char*) realloc (packdata, sizeof(unsigned char)*(3+strlen(data)));
  packtam = 3+tam+1;
  this->setTam(packtam-4);
  packcrc = 0;
  packcrc = packdata[1] xor packcrc;
  packcrc = packdata[2] xor packcrc;
  for (unsigned int i = 0; i < strlen(data); i++){
      packdata[i+3] = data[i];
      packcrc = data[i] xor packcrc;
  }
  packdata[packtam-1] = packcrc;
  if (packtam <67){
    for (int i=packtam; i<=67;i++){
      packdata[i]=0x00;
    }
    packtam=67;
  }
}

unsigned char* Pacote::getData(){
  packcrc = packdata[this->getTam()+3];
  packdata[this->getTam()+3] = 0;
  return &packdata[3];
}

void Pacote::sendPac(int soquete){
  //printf("Sending: %d\n",packtam);
  if(this->getType() == 0)
    cout << "Enviando nack" << endl;
  if(this->isValid() == false)
    cout << "Enviando pacote não valido" << endl;

  cerr << "Enviando pacote tipo: "<< this->getType() << " tamanho: " << this->getTam()<<" sequencia: " << this->getSeq() << " " << ((this->isValid() == true )?"valido":"invalido") << endl;
  send(soquete, packdata,packtam, 0);
}

void Pacote::getPac(int soquete){
  //unsigned char buffer[67];
  while(1){
    recv(soquete, &packdata, 67, 0);
    if(packdata[0] == 0x7e){
       //printf("Recieving: %d\n", tam);
      cerr << "Recebi pacote tipo: "<< this->getType() << " tamanho: " << this->getTam()<<" sequencia: " << this->getSeq() << " " << ((this->isValid() == true )?"valido":"invalido") << endl;
#ifdef LO_DEF
      recv(soquete, &packdata, 67, 0);
#endif
      return;
    }
  }
}

void Pacote::getPac(int soquete, bool timeout){
  //unsigned char buffer[67];
  struct pollfd fild;
  char dat[]="aaaa";
  fild.fd = soquete;
  fild.events = 0 | POLLIN;
  int teste;
  while(1){
    teste = poll(&fild,1, 1000);
    if (teste != POLLIN){
      cout << "Timeout" << endl;
      this->setType(0);
      this->setData(dat);
      return;
    }else{
      recv(soquete, &packdata, 67, 0);
      cerr << "Recebi pacote";
      if(packdata[0] == 0x7e){
         //printf("Recieving: %d\n", tam);
        cerr << " tipo: "<< this->getType() << " tamanho: " << this->getTam()<<" sequencia: " << this->getSeq() << " " << ((this->isValid() == true )?"valido":"invalido") << endl;
        return;
      }else{
        cout << "Pacote lixo" << endl;
      }
    }
  }
}

bool Pacote::isValid(){
  unsigned char crc =0;
  //3 4, é talvez 5
  //int tipo;
  //tipo = this->getType();
  for (int i = 0; i < this->getTam()+2; i++){
    crc = packdata[i+1] xor crc;
  }
  if (crc  != packdata[this->getTam()+3]){
    return false;
  }
  return true;
}

int Pacote::calcCrc(){
  unsigned char crc =0;
  for (int i = 0; i < this->getTam()+2; i++){
    crc = packdata[i+1] xor crc;
  }
  return (int) crc; 
}

void Pacote::setCalcCrc(){
  int crc =0;
  //unsigned char uccrc;
  crc = this->calcCrc();
  //uccrc = (unsigned char) crc;
  packdata[this->getTam()+3] = crc;
}

