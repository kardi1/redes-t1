#ifndef UTILSS_H
#define UTILSS_H
//defaults
#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include <unistd.h>
#include <queue>

#include <string>

#include "../network/network.h"
#include "../utils/utils.h"

using namespace std;

void lsServer(int, char*, unsigned char*);

void cdServer(int, char*, unsigned char*);

void getServer(int, char*, unsigned char*);
void putServer(int, char*, unsigned char*);

void RecebeArquivo(int soquete, ofstream *wri,unsigned char* Tam, unsigned char *seq);

#endif
