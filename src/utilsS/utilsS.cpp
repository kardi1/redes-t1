#include "utilsS.h"

void lsServer(int soquete, char *argumento, unsigned char *seq){
  char buff[256];
  string sbuff;
  Pacote pac;
  sprintf(buff,"ls %s > /tmp/file 2> /tmp/erro", argumento);
   
  system(buff);

  ifstream file;
  file.open("/tmp/erro");
  if(file.peek() != ifstream::traits_type::eof()){
    file.read(buff,sizeof(char)*63);
    cout<<buff;
    sbuff = buff;
    if(sbuff.find("denied")){
      pac.setSeq(*seq);
      (*seq)++;
      pac.setType(14);
      pac.setData("1");
      pac.sendPac(soquete);
    }else{
      pac.setSeq(*seq);
      (*seq)++;
      pac.setType(14);
      pac.setData("0");
      pac.sendPac(soquete);
    }
  }else{
    file.close();
    file.open("/tmp/file");
    while(file.good()){
      file.read(buff, sizeof(char)*63);
      cout << buff;
      enviaPac(soquete,buff, 10, seq);
      //(*seq)++;
      for(int i=0; i< 63; i++)
        buff[i] = 0;
    }
    pac.setSeq(*seq);
    (*seq)++;
    pac.setType(15);
    pac.setData("");
    pac.sendPac(soquete);
    file.close();
  }
}

void cdServer(int soquete, char *argumento, unsigned char *seq){
  int status;
  string mesg;
  cout << "path: " << argumento << endl;
  status=chdir(argumento);
  cout << "Status : " << status << endl;
  if(status == 0){
    mesg = "Sucesso ao altera diretorio\n";
  }else{
    mesg = "Impossivel alterar diretorio\n";
  }
  enviaPac(soquete, mesg, 10, seq);
  Pacote ftx;
  ftx.setType(15);
  ftx.setSeq(*seq);
  (*seq)++;
  ftx.setData("");
  ftx.sendPac(soquete);
}

void RecebeArquivo(int soquete, ofstream *wri,unsigned char* Tam, unsigned char *seq){

    Pacote ok, rec, error, tmp, nack,ack;
    ack.setType(1);
    ack.setData("");
    //unsigned char last;
    char lastch[3];
    lastch[0] = 0;
    lastch[1] = 0;
    lastch[2] = 0;

    
    Pacote Q[64];
    unsigned long long int fileTam, pacotes;
    int resto, cont;
    int bitmask,first;
    fileTam = atoi((const char*)Tam);
    pacotes = fileTam / 63;
    resto = 0;
    resto = fileTam % 63;
    if (resto)
      pacotes++;

    //ver quantos pacotes precisa e fazer o while corretamente
    while(pacotes > 2){
        bitmask =0;
        cont=0;
        first = -1;
        for(int j=0; j< 3; j++){
            tmp.getPac(soquete);
            if(!tmp.isValid()){
                bitmask &= ~(1<<j);
            }else{
                if(first == -1)
                    first = tmp.getSeq();
                if(tmp.getSeq() != ((*seq)&0x3f)){
                  cerr << "Entrei, tmp "<<tmp.getSeq()<<" seq " << (int)((*seq)&0x3f) << endl;
                  j--;
                  cont++;
                  bitmask |= 1<<j;
                  if(cont == 3){
                    bitmask =15;
                    j = 3;
                    (*seq)--;
                  }
                }else{
                  bitmask |= 1<<j;
                  Q[tmp.getSeq()] = tmp;
                  (*seq)++;
                  (*seq) &= 0x3f;
                }
            }
        }
        checatodos:
        if ( bitmask == 15){
            ack.setSeq(*seq);
            (*seq)++;
            ack.setCalcCrc();
            ack.sendPac(soquete);
        }else if ( bitmask == 7 ){
            //todos vieram ok, escreve
            for(int j=0; j<3; j++){
                //wri << Q[first].getData();
                wri->write((const char*)Q[first].getData(), Q[first].getTam());
                first++;
                first &= 0x3f;
            }
            pacotes -= 3;
            ack.setSeq(*seq);
            (*seq)++;
            ack.setCalcCrc();
            ack.sendPac(soquete);

        } else {
            if(!(bitmask & (1))){
                //erro no primeiro
                nack.setSeq(*seq);
                (*seq)++;
                //mais seguro se usar o first para colocar ai, pq o getSeq pode estar quebrado
                lastch[0] = (char)Q[first].getSeq();
                nack.setData(lastch);
                //manda nack no primeiro, vai receber so o primeiro passa pro
                nack.sendPac(soquete);

                tmp.getPac(soquete);
                if(!tmp.isValid()){
                    bitmask &= ~(1);
                }else{
                    bitmask |= 1;
                    Q[tmp.getSeq()] = tmp;
                }
                goto checatodos;
            }
            if(!(bitmask & (2))){
                //erro no segundo escreve o primeiro
                //wri << Q[first].getData();
                wri->write((const char*)Q[first].getData(), Q[first].getTam());
                first++;
                first &= 0x3f;
                pacotes--;
                //shift da bitmaks
                bitmask = bitmask >> 1;
                //manda nack do segundo, vai receber o segundo e o quarto,

                nack.setSeq(*seq);
                (*seq)++;
                lastch[0] = (char)Q[first].getSeq();
                nack.setData(lastch);
                nack.sendPac(soquete);
                //vai receber o segundo que agora é o primeiro, e o quarto que o é o teceiro
                //o terceiro agora é o segundo e esta provavelmente ok, isso ocorreu por causa do shift
                for(int j=0; j< 3; j++){
                  if(j == 1) continue;
                    tmp.getPac(soquete);
                    if(!tmp.isValid()){
                        bitmask &= ~(1<<j);
                    }else{
                        bitmask |= 1<<j;
                        Q[tmp.getSeq()] = tmp;
                    }
                }
                goto checatodos;
                //subitrai pacotes restante
                //pode dar problema se nao tiver pacote restante, fazer checagem
            }
            if(!(bitmask & (4))){
                for(int j=0; j<2; j++){
                    //wri << Q[first].getData();
                    wri->write((const char*)Q[first].getData(), Q[first].getTam());
                    first++;
                    first &= 0x3f;
                }
                pacotes -=2;
                //erro no terceiro
                //manda nack do terceiro e comeca o laco dnv,
                nack.setSeq(*seq);
                (*seq)++;
                lastch[0] = (char)Q[first].getSeq();
                nack.setData(lastch);
                nack.sendPac(soquete);
                //pode dar problema se nao tiver pacote restante, fazer checagem
            }
        }//fim do if checa todos
    }
    //um numero de pacotes para receber
    //no maximo 2
    //no minimo 0(se tudo tiver certo)
    //entao 1 ou 2
    bitmask =0;
    first = -1;
    unsigned int quantos = 0;
    for(unsigned int j=0; j< pacotes; j++){
        tmp.getPac(soquete);
        if(!tmp.isValid()){
            bitmask &= ~(1<<j);
        }else{
            if(first == -1)
                first = tmp.getSeq();
            bitmask |= 1<<j;
            Q[tmp.getSeq()] = tmp;
            (*seq)++;
            quantos++;
            //wri << tmp.getData();
            wri->write((const char*)tmp.getData(), tmp.getTam());
        }
    }
    cout << "Transferencia concluida" << endl;
    if(quantos == pacotes && pacotes > 0){
      ack.setSeq(*seq);
      (*seq)++;
      ack.setCalcCrc();
      ack.sendPac(soquete);
    }else{
      for(unsigned int j=0; j <quantos; j++){
        while(!(bitmask & (1<<j))){
          nack.setSeq(*seq);
          (*seq)++;
          lastch[0] = (char)first;
          nack.setData(lastch);
          nack.sendPac(soquete);
        //pede denevo
          tmp.getPac(soquete,true);
          while(tmp.getType() == 0){
            nack.sendPac(soquete);
            tmp.getPac(soquete,true);
          }
          if(!tmp.isValid()){
              bitmask &= ~(1<<j);
          }else{
              if(first == -1)
                  first = tmp.getSeq();
              bitmask |= 1<<j;
              Q[tmp.getSeq()] = tmp;
              //wri << tmp.getData();
              wri->write((const char*)tmp.getData(), tmp.getTam());
          }
        }
        first++;
        first &= 0x3f;
      }
    }
}

void putServer(int soquete, char *argumento, unsigned char *seq){
  ifstream file;
  file.open(argumento);
  if(file){
    //arquivo existe mandar erro
  //file.close
    //return
  }
  file.close();

  ofstream wri;
  wri.open(argumento, ios::binary);

  Pacote ok, rec, error, tmp, nack,ack;

  ack.setType(1);
  ack.setData("");

  if(wri){
    ok.setType(8);
    ok.setSeq(*seq);
    (*seq)++;
    ok.setData("");
    ok.sendPac(soquete);

    rec.getPac(soquete,true);
    while(rec.getType() == 0){
      ok.sendPac(soquete);
      rec.getPac(soquete,true);
    }
    (*seq)++;

    if(rec.getType() != 9){
      cout << "Fatal error" << endl;
      exit(-1);
    }
    cout << "O arquivo: "<< argumento <<" tem tamanho: "<< rec.getData() << endl;
    //extrair tamanho
    ok.setSeq(*seq);
    (*seq)++;
    ok.setCalcCrc();
    ok.sendPac(soquete);

    RecebeArquivo(soquete, &wri,rec.getData(),seq);

    tmp.getPac(soquete,true);
    while(tmp.getType() == 0){
      ok.sendPac(soquete);
      tmp.getPac(soquete,true);
    }
    (*seq)++;
    if(tmp.getType() != 15){
      cout << "Erro fatal" << endl;
    }
    ack.setSeq(*seq);
    (*seq)++;
    ack.setCalcCrc();
    ack.sendPac(soquete);

    wri.close();
  }
}

void getServer(int soquete, char* argumento, unsigned char*seq){
  struct stat statusFile;
  ifstream file;
  unsigned long long int fileTam;
  char cFileTam[64];


  if(stat(argumento, &statusFile) == -1) {
    cout << "Erro no put, arquivo nao existente ou sem permissoes" << endl;
    return;
  }

  fileTam = statusFile.st_size;
  sprintf(cFileTam,"%lld",fileTam);

  cout << "O arquivo: " << argumento << " tem tamanho: " << cFileTam << endl;

  if (!enviaPac2(soquete,cFileTam,9,seq)){
    cout << "Erro fatal"<< endl;
    return;
  }
  //comeca a transferir os roles;
  file.open(argumento, ios::binary);
  enviaArquivo(soquete, &file,seq, fileTam);
  file.close();
  enviaPac(soquete, "",15,seq);
}

