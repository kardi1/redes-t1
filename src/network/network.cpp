#include "network.h"

Pacote ACK,NACK,OK,ERROR,FTX, work;
void serverLoop(int sock){
  cerr << "Abri serverLoop" << endl;
//  int fim = 0;
  unsigned char seq=0;


  NACK.setData("");
  NACK.setType(0);
  NACK.setTam(0);

  ACK.setData("");
  ACK.setType(1);
  ACK.setTam(0);

 // OK.setType(8);
 // OK.setTam(0);

 // ERROR.setType(14);
 // ERROR.setTam(0);

 // FTX.setType(15);
 // FTX.setTam(0);

  while(1){
    work.getPac(sock);
    //cerr << "Recebi pacote tipo: "<< work.getType() << " tamanho: " << work.getTam()<<" sequencia: " << work.getSeq() << " " << ((work.isValid() == true )?"valido":"invalido") << endl;
    //cerr << "\tCom os dados: " << work.getData() << endl;

    if (work.getSeq() == seq-2){
      ACK.setSeq(work.getSeq()+1);
      ACK.setCalcCrc();
      ACK.sendPac(sock);
    }else{
      seq++;
      switch (work.getType()){
        case 3://cdServer
          cdServer(sock,(char*)work.getData(),&seq);
          break;
        case 4://lsServer
          lsServer(sock,(char*)work.getData(),&seq);
          break;
        case 5://put
          putServer(sock,(char*)work.getData(),&seq);
          break;
        case 6://get
          getServer(sock,(char*)work.getData(),&seq);
      }
    }
  }
}

void enviaPac(int sock,string arg, int type,unsigned char *seq){
  Pacote pac,rec;

  while(arg.front() == ' ') arg.erase(arg.begin());

  NACK.setData("");
  NACK.setType(0);
  NACK.setTam(0);

  ACK.setData("");
  ACK.setType(1);
  ACK.setTam(0);

  pac.setType(type);
  pac.setSeq(*seq);
  (*seq)++;
  pac.setData(arg.c_str());


  //cout << "Enviando um pacote do tipo: "<< pac.getType() << endl;
  //cout << "\tCom o tamanho: " << pac.getTam() << endl;
  //cout << "\tEsse pacote é: " << ((pac.isValid() == true )?"valido":"invalido") << endl;
  //cout << "\tCom sequencia: " << pac.getSeq() << endl;
  pac.sendPac(sock);
  rec.getPac(sock,true);
  (*seq)++;
  //cout << "recebi um pacote do tipo: "<< rec.getType() << endl;
  //cout << "\tCom o tamanho: " << rec.getTam() << endl;
  //cout << "\tCom sequencia: " << rec.getSeq() << endl;
  //cout << "\tEsse pacote é: " << ((rec.isValid() == true )?"valido":"invalido") << endl;
  //cerr << "Recebi pacote tipo: "<< rec.getType() << " tamanho: " << rec.getTam()<<" sequencia: " << rec.getSeq() << " " << ((rec.isValid() == true )?"valido":"invalido") << endl;
  while(rec.getType() == 0){
    cerr << "Reenviando pacote, recebido do tipo: " << rec.getType() << endl;
    pac.sendPac(sock);
    rec.getPac(sock, true);
  }
  while(rec.getType() == 10){
    while (!rec.isValid()){
      NACK.setSeq(*seq);
      (*seq)++;
      NACK.setCalcCrc();
      NACK.sendPac(sock);
      rec.getPac(sock, true);
    }
    cout<<rec.getData();
    ACK.setSeq(*seq);
    (*seq)++;
    ACK.setCalcCrc();
    ACK.sendPac(sock);
    rec.getPac(sock, true);
    (*seq)++;
  }

  if (rec.getType() == 14){
    cout << "Erro ao executar o ls" << endl;
  }
}

bool enviaPac2(int sock,string arg, int type,unsigned char *seq){
  Pacote pac,rec;

  while(arg.front() == ' ') arg.erase(arg.begin());

  NACK.setData("");
  NACK.setType(0);
  NACK.setTam(0);

  ACK.setData("");
  ACK.setType(1);
  ACK.setTam(0);

  pac.setType(type);
  pac.setSeq(*seq);
  (*seq)++;
  pac.setData(arg.c_str());

  pac.sendPac(sock);
  rec.getPac(sock, true);
  (*seq)++;

  while(rec.getType() == 0){
    cout << "Reenviando pacote, recebido do tipo: " << rec.getType() << endl;
    pac.sendPac(sock);
    rec.getPac(sock,true);
  }
  if (rec.getType() == 14){
    cout << "Erro ao executar" << endl;
    return false;
  }
  return true;
}
