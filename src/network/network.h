#ifndef NETWORK_H
#define NETWORK_H
//socket
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/ethernet.h>
#include <linux/if_packet.h>
#include <linux/if.h>
#include <arpa/inet.h>

//defaults
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <string>
#include <cstdio>

//error
#include <errno.h>
#include <unistd.h>

#include "../pacote/pacote.h"
#include "../utilsS/utilsS.h"

void serverLoop(int);

void enviaPac(int, string, int, unsigned char*);

bool enviaPac2(int, string, int, unsigned char*);

#endif
