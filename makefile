all:
	@echo " "
	@echo " "
	@echo "Compilando servidor"
	make -f mk-server
	@echo " "
	@echo " "
	@echo "Compilando cliente"
	make -f mk-cliente

server:
	make -f mk-server

cliente:
	make -f mk-cliente
